import { Injectable } from '@angular/core';
import { fromEvent, ConnectableObservable, Subject } from 'rxjs';
import { publishReplay } from 'rxjs/operators';
import { ClickValues, ClickValueType } from '../types/click-values';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private _socket: SocketIOClient.Socket;
  public onClickValueChanged: ConnectableObservable<ClickValues>;

  constructor() {
    this.initSocket();
  }

  private initSocket() {
    if (!this._socket) {
      this._socket = io(`${window.location.hostname}:3000`);

      this.attachWebsocketStreams();
      this.handleDisconnectInitiatedFromServer();
    }
  }

  private handleDisconnectInitiatedFromServer() {
    fromEvent<string>(this._socket, 'disconnect').subscribe(reason => {
      if (reason === 'io server disconnect') {
        this._socket.connect();
      }
    });
  }

  private attachWebsocketStreams() {
    this.onClickValueChanged = this.clickValueChanged();

    this.onClickValueChanged.connect();
  }

  private clickValueChanged(): ConnectableObservable<ClickValues> {
    return fromEvent<ClickValues>(this._socket, 'clickValueChanged').pipe(
      // multicast(new ReplaySubject(1))
      // same thing as:
      publishReplay(1)
    ) as ConnectableObservable<ClickValues>;
  }

  public incrementClickValue(clickValueType: ClickValueType) {
    this.sendEvent('incrementClickValue').next(clickValueType);
  }

  private sendEvent(eventName: string) {
    const observer = {
      next: (clickValueType: ClickValueType) => {
        this._socket.emit(eventName, clickValueType);
      }
    };

    return Subject.create(observer);
  }
}
