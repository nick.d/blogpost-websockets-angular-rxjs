import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SocketService } from './services/socket.service';
import { ClickValues, ClickValueType } from './types/click-values';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  clickValuesStream$: Observable<ClickValues>;
  relativePercentageClickValuesStream$: Observable<ClickValues>;

  constructor(private socketService: SocketService) {}

  ngOnInit() {
    this.initClickValuesStream();
    this.convertClickValuesToPercentageStream();
  }

  private convertClickValuesToPercentageStream() {
    this.relativePercentageClickValuesStream$ = this.clickValuesStream$.pipe(
      map((values: ClickValues) => {
        const relativePercentages: ClickValues = {
          red: (values.red / (values.red + values.blue)) * 50,
          blue: (values.blue / (values.red + values.blue)) * 50
        };

        return relativePercentages;
      })
    );
  }

  private initClickValuesStream() {
    this.clickValuesStream$ = this.socketService.onClickValueChanged;
  }

  onRedClicked(): void {
    this.socketService.incrementClickValue(ClickValueType.RED);
  }

  onBlueClicked(): void {
    this.socketService.incrementClickValue(ClickValueType.BLUE);
  }
}
