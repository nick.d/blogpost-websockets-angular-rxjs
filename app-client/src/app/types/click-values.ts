export interface ClickValues {
  readonly red: number;
  readonly blue: number;
}

export enum ClickValueType {
  RED = 'RED',
  BLUE = 'BLUE'
}
