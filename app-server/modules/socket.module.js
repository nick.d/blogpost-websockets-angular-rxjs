var redValue = 0;
var blueValue = 0;

function socketHandler(socket, io) {
  socket.emit('clickValueChanged', { red: redValue, blue: blueValue });

  socket.on('incrementClickValue', handleIncrementClickValue);

  function handleIncrementClickValue(type) {
    switch (type) {
      case 'RED':
        redValue++;
        break;
      case 'BLUE':
        blueValue++;
        break;
    }

    io.emit('clickValueChanged', { red: redValue, blue: blueValue });
  }
}

module.exports = socketHandler;
